# Improved PowerShell Setup Guide

This guide will walk you through enhancing your PowerShell experience by installing Nerd Fonts and Oh-My-Posh, a custom prompt theme engine. These steps will help you achieve a more visually appealing and functional PowerShell environment.

## 1. Install Nerd Fonts

### 1.1 Choose a Nerd Font
Visit the Nerd Fonts GitHub repository: [Nerd Fonts GitHub](https://github.com/ryanoasis/nerd-fonts/) to explore the available fonts. Select the font that suits your preference.

### 1.2 Download the Font
- Go to the GitHub releases page of your chosen font.
- Download the font file in the `.zip` format.

### 1.3 Extract and Install the Font
- Extract the downloaded `.zip` file to a location on your computer.
- Browse to the extracted font files.
- Select all font files, right-click, and choose "Install".

## 2. Configure Windows PowerShell

### 2.1 Open PowerShell Settings
- Search for "terminal" in the Start Menu and open Windows PowerShell.
- Click on the drop-down arrow in the PowerShell window's title bar.
- Select "Settings" from the menu.

### 2.2 Configure Default Profile Appearance
- In the "Settings" window, navigate to the "Profiles" section.
- Select the "default" profile.
- Scroll down to the "Appearance" settings.

### 2.3 Choose Nerd Font
- Choose the Nerd Font you installed from the font list.
- The font will be listed with its name followed by "Nerd Font".

### 2.4 Restart PowerShell
- Close the PowerShell window.
- Reopen PowerShell to see the new font in action.

## 3. Install Oh-My-Posh

### 3.1 Install Oh-My-Posh
- Open a PowerShell window.
- Enter the following command to install Oh-My-Posh using the Windows Package Manager (winget):
```powershell
winget install JanDeDobbeleer.OhMyPosh -s winget
```
### 3.2 Update Oh-My-Posh
- To keep Oh-My-Posh up-to-date, use the following command:
```powershell 
winget upgrade JanDeDobbeleer.OhMyPosh -s winget
```

### 3.3 Default Themes
- Oh-My-Posh themes are located in the folder indicated by the environment variable `POSH_THEMES_PATH`.
- To initialize a theme in PowerShell, use the following command (replace `jandedobbeleer.omp.json` with your desired theme):

```powershell
oh-my-posh init pwsh --config "$env:POSH_THEMES_PATH\jandedobbeleer.omp.json"
```

## 4. Change Oh-My-Posh Prompt

To change the prompt using Oh-My-Posh, follow these steps:

- Run the following commands in PowerShell:

  ```powershell
  oh-my-posh get shell
  notepad $PROFILE
    ```
- If notepad gives an error, use the following command to create a new profile:
    ```powershell
    New-Item -Path $PROFILE -Type File -Force
    ```
- Initialize Oh-My-Posh prompt by executing:
    ```powershell
    oh-my-posh init pwsh | Invoke-Expression
    ```
- Load the updated profile:
    ```powershell
    . $PROFILE
    ```

## 5. Customise 

The standard initialization sets Oh My Posh' default theme. This configuration is embedded and thus kept up-to-date with Oh My Posh.

- Run the following command in powershell:
    ```powershell
    oh-my-posh init pwsh --config 'C:\Users\Posh\AppData\Local\Programs\oh-my-posh\themes\jandedobbeleer.omp.json' | Invoke-Expression
    ```

- To persist a config in powershell add the following line to your profile
    ```powershell
    oh-my-posh init pwsh --config 'C:\Users\Posh\AppData\Local\Programs\oh-my-posh\themes\rudolfs-dark.omp.json' | Invoke-Expression
    ```

- Once altered, reload your profile for the changes to take effect.
    ```powershell
    . $PROFILE
    ````